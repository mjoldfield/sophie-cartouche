// text_on_cube.scad - Example for text() usage in OpenSCAD

echo(version=version());

font = "Liberation Sans"; //["Liberation Sans", "Liberation Sans:style=Bold", "Liberation Sans:style=Italic", "Liberation Mono", "Liberation Serif"]

slab_size   = 25;
slab_height = 5;
letter_size = 15;
letter_height = 2;

round_rad = 0.5;

o = slab_height / 2 - letter_height / 2;

msg = [ "S", "O", "P" ];

module letter(l) {
  // Use linear_extrude() to make the letters 3D objects as they
  // are only 2D shapes when only using text()
  linear_extrude(height = letter_height) {
    text(l, size = letter_size, font = font, halign = "center", valign = "center", $fn = 16);
  }
}

difference() {
  union() {
    color("gray") 
      minkowski() 
      { 
          union() 
          {
            l = slab_size * len(msg);
            cube([slab_size, l, slab_height], center = true);
            translate([0,  0.5 * l, 0]) cylinder(h=slab_height, r = 0.5 * slab_size, center = true);
            translate([0, -0.5 * l, 0]) cylinder(h=slab_height, r = 0.5 * slab_size, center = true);
          }
          sphere(round_rad);
      }
    
   }

  // Put some symbols on top and bottom using symbols from the
  // Unicode symbols table.
  // (see https://en.wikipedia.org/wiki/Miscellaneous_Symbols)
  //
  // Note that depending on the font used, not all the symbols
  // are actually available.
  nchar = len(msg);
  for(i = [0:nchar])
  {
    translate([0, (0.5 * (nchar - 1) - i) * slab_size, o])  letter(msg[i]);
  }
}



// Written in 2014 by Torsten Paul <Torsten.Paul@gmx.de>
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to the
// public domain worldwide. This software is distributed without any
// warranty.
//
// You should have received a copy of the CC0 Public Domain
// Dedication along with this software.
// If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
