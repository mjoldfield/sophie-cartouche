echo(version=version());

letter_size = 15;
letter_depth = 1.0;

module draw_s() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-42,-25,0])
    linear_extrude(height=2 * letter_depth, center=true)
    import(file="icons/s.svg", center="true");
}
module draw_o() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-30,-38,0])
    linear_extrude(height=2 * letter_depth, center=true)
    import(file="icons/o.svg", center="true");
}
module draw_p() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-24.5,-25,0])
    linear_extrude(height=2 * letter_depth, center=true)
    import(file="icons/p.svg", center="true");
}
module draw_h() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-15,-40,0])
    linear_extrude(height=2 * letter_depth, center=true)
    import(file="icons/h.svg", center="true");
}
module draw_i() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-30,-20,0])
    linear_extrude(height=2 * letter_depth, center=true)
    import(file="icons/i.svg", center="true");
}
module draw_e() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-20,-30,0])
    linear_extrude(height=2 * letter_depth, center=true)
    import(file="icons/e.svg", center="true");
}

round_rad = 0.5;

module slab(w,l,h) {    
  union() {
    color("gray") 
      minkowski() 
      { 
          union() 
          {
            cube([w, l, h], center = true);
            translate([0,  0.5 * l, 0]) cylinder(h=h, r = 0.5 * w, center = true);
            translate([0, -0.5 * l, 0]) cylinder(h=h, r = 0.5 * w, center = true);
          }
          sphere(round_rad);
      }
    
   }
}

    



difference() {
   slab_height = 5;
    
   slab(25.0, 140.0, slab_height);    

   h = slab_height / 2.0 + round_rad;

   translate([0,  62.5, h]) draw_s();
   translate([0,  37.5, h]) scale([1.5, 1.5, 1.0]) draw_o();
   translate([0,  12.5, h]) scale([1.5, 1.5, 1.0]) draw_p();
   translate([0, -12.5, h]) scale([1.5, 1.5, 1.0]) draw_h();
   translate([0, -37.5, h]) scale([1.5, 1.5, 1.0]) draw_i();
   translate([0, -62.5, h]) scale([1.5, 1.5, 1.0]) draw_e();
}


