

echo(version=version());

font = "Liberation Sans"; //["Liberation Sans", "Liberation Sans:style=Bold", "Liberation Sans:style=Italic", "Liberation Mono", "Liberation Serif"]

slab_size   = 25;
slab_height = 5;
letter_size = 15;
letter_height = 1.0;

round_rad = 0.5;

o = slab_height / 2 + round_rad;

msg = [ "s", "o", "p", "h", "i", "e" ];

module gly_s() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-42,-25,0])
    linear_extrude(height=2 * letter_height, center=true)
    import(file="icons/s.svg", center="true");
}
module gly_o() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-30,-38,0])
    linear_extrude(height=2 * letter_height, center=true)
    import(file="icons/o.svg", center="true");
}
module gly_p() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-24.5,-25,0])
    linear_extrude(height=2 * letter_height, center=true)
    import(file="icons/p.svg", center="true");
}
module gly_h() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-15,-40,0])
    linear_extrude(height=2 * letter_height, center=true)
    import(file="icons/h.svg", center="true");
}
module gly_i() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-30,-20,0])
    linear_extrude(height=2 * letter_height, center=true)
    import(file="icons/i.svg", center="true");
}
module gly_e() {
    gscale = letter_size / 60.0;
    scale([gscale, gscale, 1.0])
    translate([-20,-30,0])
    linear_extrude(height=2 * letter_height, center=true)
    import(file="icons/e.svg", center="true");
}

module glyph(l) {
    if      (l == "s") { gly_s(); }
    else if (l == "o") { gly_o(); }
    else if (l == "p") { gly_p(); }
    else if (l == "h") { gly_h(); }
    else if (l == "i") { gly_i(); }
    else if (l == "e") { gly_e(); }

}

difference() {
  union() {
    color("gray") 
      minkowski() 
      { 
          union() 
          {
            l = slab_size * len(msg);
            cube([slab_size, l, slab_height], center = true);
            translate([0,  0.5 * l, 0]) cylinder(h=slab_height, r = 0.5 * slab_size, center = true);
            translate([0, -0.5 * l, 0]) cylinder(h=slab_height, r = 0.5 * slab_size, center = true);
          }
          sphere(round_rad);
      }
    
   }

  // Put some symbols on top and bottom using symbols from the
  // Unicode symbols table.
  // (see https://en.wikipedia.org/wiki/Miscellaneous_Symbols)
  //
  // Note that depending on the font used, not all the symbols
  // are actually available.
  nchar = len(msg);
  for(i = [0:nchar-1])
  {
    translate([0, (0.5 * (nchar - 1) - i) * slab_size, o]) glyph(msg[i]) ;
  }
}


